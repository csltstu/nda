#!/bin/bash

stage=$1

data_dir=data/lda-x-vector

# Kaldi LDA_PLDA wth PLDA scoring
if [ $stage -le 0 ]; then
  mkdir -p scores/kaldi_lda_plda_without_norm
  local/score/plda_scoring.sh --normalize-length false --simple-length-norm false \
      xvector.scp $data_dir/train_combined_800s \
      $data_dir/sitw_eval_enroll $data_dir/sitw_eval_test \
      $data_dir/sitw_eval_test/core-core.lst \
      scores/kaldi_lda_plda_without_norm

  mkdir -p scores/kaldi_lda_plda_standard_norm
  local/score/plda_scoring.sh --normalize-length true --simple-length-norm false \
      xvector.scp $data_dir/train_combined_800s \
      $data_dir/sitw_eval_enroll $data_dir/sitw_eval_test \
      $data_dir/sitw_eval_test/core-core.lst \
      scores/kaldi_lda_plda_standard_norm

  mkdir -p scores/kaldi_lda_plda_simple_norm
  local/score/plda_scoring.sh --normalize-length true --simple-length-norm true \
      xvector.scp $data_dir/train_combined_800s \
      $data_dir/sitw_eval_enroll $data_dir/sitw_eval_test \
      $data_dir/sitw_eval_test/core-core.lst \
      scores/kaldi_lda_plda_simple_norm
fi


# Kaldi LDA_PLDA with NL scoring
if [ $stage -le 1 ]; then
  mkdir -p scores/nl_lda_plda_without_norm
  python -u local/score/plda_score_by_trials.py \
      --score scores/nl_lda_plda_without_norm/lda_plda_scores \
      --enroll-npz $data_dir/sitw_eval_enroll/xvector.npz \
      --enroll-num-utts $data_dir/sitw_eval_enroll/num_utts.ark \
      --test-npz $data_dir/sitw_eval_test/xvector.npz \
      --trials $data_dir/sitw_eval_test/core-core.lst \
      --plda-W $data_dir/train_combined_800s/plda_W \
      --plda-b $data_dir/train_combined_800s/plda_b \
      --plda-SB $data_dir/train_combined_800s/plda_SB

  mkdir -p scores/nl_lda_plda_standard_norm
  python -u local/score/plda_score_by_trials.py \
      --score scores/nl_lda_plda_standard_norm/lda_plda_scores \
      --enroll-npz $data_dir/sitw_eval_enroll/xvector.npz \
      --enroll-num-utts $data_dir/sitw_eval_enroll/num_utts.ark \
      --test-npz $data_dir/sitw_eval_test/xvector.npz \
      --trials $data_dir/sitw_eval_test/core-core.lst \
      --plda-W $data_dir/train_combined_800s/plda_W \
      --plda-b $data_dir/train_combined_800s/plda_b \
      --plda-SB $data_dir/train_combined_800s/plda_SB \
      --normalize-length

  mkdir -p scores/nl_lda_plda_simple_norm
  python -u local/score/plda_score_by_trials.py \
      --score scores/nl_lda_plda_simple_norm/lda_plda_scores \
      --enroll-npz $data_dir/sitw_eval_enroll/xvector.npz \
      --enroll-num-utts $data_dir/sitw_eval_enroll/num_utts.ark \
      --test-npz $data_dir/sitw_eval_test/xvector.npz \
      --trials $data_dir/sitw_eval_test/core-core.lst \
      --plda-W $data_dir/train_combined_800s/plda_W \
      --plda-b $data_dir/train_combined_800s/plda_b \
      --plda-SB $data_dir/train_combined_800s/plda_SB \
      --normalize-length \
      --simple-length-norm
fi
