# NDA (Neural discriminant analysis with NF)

## Dependency
```
pip3 install -r local/requrements.txt
```

## Data
data preparation from kaldi to npz
```
python -u local/kaldi2npz.py
```

## Main
recipe to train and infer NDA models.
```
sh run.sh
```

## Tensorboard
monitor the training process.
```
tensorboard --logdir runs/*
```
