#!/bin/bash

. ./path.sh

stage=0

# configs of NF
flow=linear     # e.g., realnvp, linear
num_blocks=0    # no. of blocks
num_hidden=512
num_inputs=512
batch_size=800  # e.g., 200
epochs=5001
lr=0.01         # 0.0001
device=0        # gpu id
model=init_lda_xvec_spk800_${flow}_b${num_blocks}_h${num_hidden}_b${batch_size}_e${epochs}_lr${lr}
ckpt_dir=ckpt/$model
log_dir=$ckpt_dir/log


# NDA inference
if [ $stage -le 0 ]; then
  # NDA configs
  echo $model
  echo "Start to infer data from x space to z space and store to numpy npz"
  for ((infer_epoch=0; infer_epoch<${epochs}; infer_epoch=infer_epoch+100)); do
    echo $infer_epoch
    for sub in train_combined_800s sitw_eval_enroll sitw_eval_test; do
      echo $sub
      test_data_npz=data/lda-x-vector/$sub/xvector.npz
      npz_dir=$ckpt_dir/$infer_epoch/$sub

      # This may be a bug in pyTorch... you must assign GPU device before the main.py.
      CUDA_VISIBLE_DEVICES=$device python -u nda/main.py \
             --eval \
             --test-data-npz $test_data_npz \
             --flow $flow \
             --num-blocks $num_blocks \
             --num-hidden $num_hidden \
             --num-inputs $num_inputs \
             --batch-size 2000 \
             --device 0 \
             --ckpt-dir $ckpt_dir \
             --infer-epoch $infer_epoch \
             --SB-file $ckpt_dir/$infer_epoch/SB \
             --npz-dir $npz_dir/xvector.npz

      cp data/x-vector/$sub/{spk2utt,utt2spk,num_utts.ark} $npz_dir/
    done
  done

  echo "End ..."
fi


# Back-end Scoring
if [ $stage -le 1 ]; then
  echo $model
  echo "Start to infer data from x space to z space and store to numpy npz"
  for ((infer_epoch=0; infer_epoch<${epochs}; infer_epoch=infer_epoch+100)); do
    echo $infer_epoch
    score_dir=scores/$model/$infer_epoch
    mkdir -p $score_dir
    data_dir=$ckpt_dir/$infer_epoch
  
    python -u local/score/nda_score_by_trials.py \
        --score $score_dir/nl.foo \
        --enroll-npz $data_dir/sitw_eval_enroll/xvector.npz \
        --enroll-num-utts $data_dir/sitw_eval_enroll/num_utts.ark \
        --test-npz $data_dir/sitw_eval_test/xvector.npz \
        --trials data/lda-x-vector/sitw_eval_test/core-core.lst \
        --nda-SB $data_dir/SB
  
    python -u local/score/nda_score_by_trials.py \
        --score $score_dir/nl_standard_norm.foo \
        --enroll-npz $data_dir/sitw_eval_enroll/xvector.npz \
        --enroll-num-utts $data_dir/sitw_eval_enroll/num_utts.ark \
        --test-npz $data_dir/sitw_eval_test/xvector.npz \
        --trials data/lda-x-vector/sitw_eval_test/core-core.lst \
        --nda-SB $data_dir/SB \
        --normalize-length
  
    python -u local/score/nda_score_by_trials.py \
        --score $score_dir/nl_simple_norm.foo \
        --enroll-npz $data_dir/sitw_eval_enroll/xvector.npz \
        --enroll-num-utts $data_dir/sitw_eval_enroll/num_utts.ark \
        --test-npz $data_dir/sitw_eval_test/xvector.npz \
        --trials data/lda-x-vector/sitw_eval_test/core-core.lst \
        --nda-SB $data_dir/SB \
        --normalize-length \
        --simple-length-norm
  done
fi
